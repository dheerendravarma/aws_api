import boto3


ec2 = boto3.resource('ec2')

'''ids = ['i-0cbf703884b55df75', 'i-04de382d0c50d440e']
ec2.instances.filter(InstanceIds=ids).stop()
ec2.instances.filter(InstanceIds=ids).start()'''

instances = ec2.instances.filter(Filters=[{'Name': 'instance-state-name',
                                           'Values': ['stopped']}])
for instance in instances:
    print(instance.id, instance.instance_type)
