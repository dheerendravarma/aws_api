import boto3


def get_region_names():
    """Fetch and return the region list

    Args:
        None.

    Returns:
        list:returns list of region names.

    """
    ec2_client = boto3.client('ec2')
    regions = ec2_client.describe_regions()['Regions']
    region_names = [region["RegionName"] for region in regions]
    return region_names


def get_reg_availzones(region_names):
    """Returns the dictionary of all the availability
       zones for each zone.

    Args:
        region_names (list): Names of all the zones.

    Returns:
        dict:Dictionary of the zones and the corres-
            ponding availability zones.
    """
    reg_avail = {key: [] for key in region_names}
    for region_name in region_names:
        ec2_client = boto3.client('ec2', region_name=region_name)
        for az in ec2_client.describe_availability_zones()['AvailabilityZones'
                                                           ]:
            reg_avail[az['RegionName']].append(az['ZoneName'])
    return reg_avail


def get_Instances(region_name):
    """Returns the Instance IDS for instances
       available in the given region_name.

    Args:
        region_name (str):Name of the Region.

    Returns:
        list: List of Instance ID's in string format.
    """
    ec2_client = boto3.client('ec2', region_name=region_name)
    instances = ec2_client.describe_instances()['Reservations']
    instance_ids = [instance['Instances'][0]['InstanceId'] for
                    instance in instances]
    return instance_ids


region_names = get_region_names()
for region_name in region_names:
    print("Verifying %s" % (region_name))
    instance_ids = get_Instances(region_name)
    if len(instance_ids) == 0:
        print("There are no available Instances in the Region %s!!" %
              (region_name))
    else:
        print(":::Instance IDS in region %s:::" % (region_name,))
        print(instance_ids)
