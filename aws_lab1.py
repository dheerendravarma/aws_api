import boto3


def create_instances(region, AMI_ID, Instance_Count, Instance_Type):
    """Returns instance ids of the instances.

        Based on the input parameters instances created
        and the remaining parameters are set as default.

    Args:
        region(str) : Name of the region.
        AMI_ID(str) : Amazon Machine image ID.
        Instance_Count(int) : Number of instances to be craeted.
        Instance_Type(str) : Type of the Processor.

    Returns:
        list: List of Instance ID's in string format.
    """
    ec2 = boto3.resource('ec2', region_name=region)
    instance = ec2.create_instances(ImageId=AMI_ID,
                                    MinCount=1,
                                    MaxCount=Instance_Count,
                                    InstanceType=Instance_Type,
                                    KeyName='dheerendra',
                                    SecurityGroupIds=['sg-1c5bad6f'],
                                    Monitoring={'Enabled': True})

    instance_ids = [instance[i].instance_id for i in range(len(instance))]
    ec2.create_tags(Resources=instance_ids, Tags=[{'Key': 'Name', 'Value':
                                                   'dheeru'}])
    return (instance_ids)


region = input("Enter Region Name::")
AMI_ID = input("Enter AMI ID::")
Instance_Count = int(input("How many instances you want::"))
Instance_Type = input("Type of instance You want::")
instance_ids = create_instances(region, AMI_ID, Instance_Count, Instance_Type)
print(instance_ids)
